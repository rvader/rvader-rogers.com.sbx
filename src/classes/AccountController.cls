public with sharing class AccountController {
	
	// some comments
	// others

    @AuraEnabled
    public static List<Account> findAll() {
    return [SELECT id, name, Location__Latitude__s, Location__Longitude__s
            FROM Account
            WHERE Location__Latitude__s != NULL AND Location__Longitude__s != NULL
            LIMIT 50];
    }
    
    
    

	@AuraEnabled
    public static Account findById(String accountId) {
	    return [Select Id, Website, Type, ShippingCity, ShippingAddress, RecordTypeId, RecordType.Name, Rating, Phone, BillingCity, BillingAddress
                		, AccountNumber, Name  From Account
        				where Id = :accountId];
	}    

}