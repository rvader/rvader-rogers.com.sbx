public class Lookup0002_ctrl {
    
    @AuraEnabled
    public static List < account > fetchAccount(String searchKeyWord) {
        String searchKey = searchKeyWord + '%';
        List < Account > returnList = new List < Account > ();
        
        List<Account> lstOfAccount = [Select Id, Website, Type, RecordTypeId, RecordType.Name
                                      , Rating, Phone, AccountNumber, Name  From Account where Name like :searchkey];
        
        // List < Account > lstOfAccount = [select id, Name from account where Name LIKE: searchKey];
        
        for (Account acc: lstOfAccount) {
            returnList.add(acc);
        }
        return returnList;
    }
}