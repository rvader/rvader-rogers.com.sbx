public class LxLookupApexController {
    
    @AuraEnabled
    public static List < User > fetchRecordsByKeyword(String searchKeyWord) {
        String searchKey = searchKeyWord + '%';
        List < User > returnList = new List < User > ();
        
        List<User> lsRecords = [select Id, Name, LastName, FirstName from User where LastName like :searchkey];
        
        for (User u: lsRecords) {
            returnList.add(u);
        }
        return returnList;
    }
}