public class WorkOrderMobileController {


    
    @AuraEnabled
    public static Case getCaseRecord(String recordId) {
        return [select Id, CaseNumber, Status, Priority, Type, RecordTypeId, Subject from Case where Id = :recordId limit 1];
    }

}