public with sharing class CaseEntryItemsController {
  @AuraEnabled
  public static List<CaseEntryItem__c> getCaseEntryItemsByParent( Id parentId) {
  	
  
    return [Select w.wrv2__Notes__c, w.wrv2__Hours__c, w.wrv2__Description__c, w.wrv2__Date__c, w.wrv2__Current_Week__c, w.wrv2__Case_Entry__c, w.Name, w.Id 
    			From wrv2__CaseEntryItem__c w
    			where w.wrv2__Case_Entry__c = :parentId
    			ORDER BY createdDate desc];
  }
}