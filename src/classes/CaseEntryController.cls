public class CaseEntryController{
  @AuraEnabled
  public static List<CaseEntry__c> getCaseEntries() {
    return [SELECT Id, OwnerId, Name, wrv2__Case_Date__c, wrv2__Title__c, wrv2__Description__c, wrv2__Priority__c, wrv2__Stage__c, wrv2__Assigned_To__c, wrv2__Total_Hours__c, wrv2__Hours_This_Week__c FROM wrv2__CaseEntry__c ORDER BY createdDate desc];
  }
}